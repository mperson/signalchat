﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace RepoChat.Controllers
{
    public class DataController : ApiController
    {
         public string Get()
        {
            return "Hello";
        }

        // POST api/<controller>
        public HttpResponseMessage PostFile()
        {
            HttpResponseMessage result = null;
            
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                var docfiles = new List<string>();
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    var filePath = HttpContext.Current.Server.MapPath("~/images/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);
                    docfiles.Add(postedFile.FileName);
                }
                result = Request.CreateResponse(HttpStatusCode.Created, docfiles[0]);
            }
            else
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return result;
        }


    }
}