﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace RepoChat.hubs
{
    public class ChatHub : Hub
    {
        public void sendMessage(string Message)
        {
            string info =   Message;

            Clients.Others.getMessage(info);
            Clients.Caller.getPersonalMessage(Message);
        }

        public void sendImage(string imageName)
        {

            Uri local = (Context.Request.Environment[typeof(HttpContextBase).FullName] as HttpContextBase).Request.Url;
            string path = "http://" + local.Host;
            if (local.Port != 80)
            {
                path += ":" + local.Port;
            }
            path += "/images/" + imageName.Replace("\"",""); 

            Clients.Others.getImage(path);
            Clients.Caller.getPersonalImage(path);
        }
    }


}